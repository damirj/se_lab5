# Damir Jurković

## Labos 5

1. Napravili smo novi repozitorij na GitLab-u (se_lab5)
1. Pokrenuli VirtualBox
1. Napravili svoju mapu preko terminala 
1. klonirali git repo u nasu mapu (__git clone__)
1. napravili novi file test.py (__touch test.py__)
1. napravili novi file README.me (__touch README.me__)
1. commitali promjenu (__git commit -m__)
1. pushali na remote repo (__git push origin master__)
